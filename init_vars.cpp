#include <iostream>


// // Task 1 (short int, int, long long, char, bool, float, double).
int main()
{
    short int si(5);
    int a(10);
    long long ll_var (654`122);
    char word('w');
    bool(true);
    float f_var = 9.17f;
    double dd_var = 6E-5;
    int arr [] = {3, 4, 7, 9, 11, 5};
    return 0;

}

const int LONG_ARR = 9;
const int SHORT_ARR = 3;

{
    // Task 2
    enum XO_Game { X, O, empty };

    // Task 3
    XO_Game game_arr[LONG_ARR] = {empty, empty, empty, empty, empty, empty, empty, empty, empty};

    // Task 4.
    struct XO_Game {

      XO_Game game_line_1[SHORT_ARR] = {empty, empty, empty};
      XO_Game game_line_2[SHORT_ARR] = {empty, empty, empty};
      XO_Game game_line_3[SHORT_ARR] = {empty, empty, empty};
    };


 // Task 5
    {
      union MyData {
         int i_data;
         float f_data;
         char c_data;
    };

      struct MyVariant {
          MyData md_data;
          unsigned short isInt : 2;
          unsigned short isFloat : 4;
          unsigned short isChar : 1;
     };

      // Example
      auto some_var = new Custom_Var {{9}, 0, 0, 1 };

      SetValue(some_var, 3.4f);
      std::cout << Custom_Var(some_var) << std::endl;
      SetValue(some_var, 'R');
      std::cout << Custom_Var(some_var) << std::endl;
      SetValue(some_var, 999);
      std::cout << Custom_Var(some_var) << std::endl;
    }
  }